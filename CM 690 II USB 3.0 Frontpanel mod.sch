EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB3_A USB3_1
U 1 1 5FA3D645
P 1700 1850
F 0 "USB3_1" H 1757 2567 50  0000 C CNN
F 1 "USB3_A" H 1757 2476 50  0000 C CNN
F 2 "CM 690 II USB 3.0 Frontpanel mod:USB3_A_Hroparts_Elec_U-A-39DD-Y-2" H 1850 1950 50  0001 C CNN
F 3 "~" H 1850 1950 50  0001 C CNN
F 4 "C127375" H 1700 1850 50  0001 C CNN "LCSC"
	1    1700 1850
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB3_A USB3_2
U 1 1 5FA42683
P 3100 1850
F 0 "USB3_2" H 3157 2567 50  0000 C CNN
F 1 "USB3_A" H 3157 2476 50  0000 C CNN
F 2 "CM 690 II USB 3.0 Frontpanel mod:USB3_A_Hroparts_Elec_U-A-39DD-Y-2" H 3250 1950 50  0001 C CNN
F 3 "~" H 3250 1950 50  0001 C CNN
F 4 "C127375" H 3100 1850 50  0001 C CNN "LCSC"
	1    3100 1850
	1    0    0    -1  
$EndComp
Text Notes 1200 1000 0    50   ~ 0
USB 3.0 Connectors, they will fit in the default location
Wire Notes Line
	7800 4350 4550 4350
Wire Notes Line
	4550 1100 7800 1100
Text Notes 4600 1050 0    50   ~ 0
USB C connectors, they will fit in the eSATA port, which nobody ever used
Text Notes 8200 1600 0    50   ~ 0
Speaker pinout (bottom seen)\n     2: Left \n     3: Detect 1\n1: Sleeve (ground)\n     4: Detect 2\n     5: Right
Text Notes 9600 1600 0    50   ~ 0
Microphone pinout (bottom seen)\n     2: Left \n     3: Detect 1\n1: Sleeve (ground)\n     4: Detect 2\n     5: Right
Text Notes 8200 3150 0    50   ~ 0
Audio connector: (seen from the bottom, with hp connector facing left)\n                \n2: NC                     1: SP_DETECT_1\n4: SP_DETECT_2           3: SP_LEFT\n6: SP_RIGHT               5: AGND\n8: MIC_LEFT               7: MIC_RIGHT\n10: MIC_DETECT_1         9: MIC_DETECT_2
Text GLabel 8850 1900 2    50   Input ~ 0
A_GND
Text GLabel 10200 1900 2    50   Input ~ 0
A_GND
Text GLabel 8850 2000 2    50   Input ~ 0
SP_RIGHT
Text GLabel 8850 2200 2    50   Input ~ 0
SP_LEFT
Text GLabel 8850 2300 2    50   Input ~ 0
SP_DETECT_1
Text GLabel 8850 2100 2    50   Input ~ 0
SP_DETECT_2
Text GLabel 10200 2000 2    50   Input ~ 0
MIC_RIGHT
Text GLabel 10200 2100 2    50   Input ~ 0
MIC_DETECT_2
Text GLabel 10200 2200 2    50   Input ~ 0
MIC_LEFT
Text GLabel 10200 2300 2    50   Input ~ 0
MIC_DETECT_1
Text GLabel 9150 3600 2    50   Input ~ 0
SP_DETECT_1
NoConn ~ 9150 3500
Text GLabel 9150 3800 2    50   Input ~ 0
SP_LEFT
Text GLabel 9150 3700 2    50   Input ~ 0
SP_DETECT_2
Text GLabel 9150 4000 2    50   Input ~ 0
A_GND
Text GLabel 9150 3900 2    50   Input ~ 0
SP_RIGHT
Text GLabel 9150 4200 2    50   Input ~ 0
MIC_RIGHT
Text GLabel 9150 4100 2    50   Input ~ 0
MIC_LEFT
Text GLabel 9150 4400 2    50   Input ~ 0
MIC_DETECT_2
Text GLabel 9150 4300 2    50   Input ~ 0
MIC_DETECT_1
Wire Notes Line
	8050 1100 11000 1100
Wire Notes Line
	11000 1100 11000 4550
Wire Notes Line
	11000 4550 8050 4550
Wire Notes Line
	8050 4550 8050 1100
Text Notes 8750 1050 2    50   ~ 0
Front panel audio
Text GLabel 1750 3600 1    50   Input ~ 0
Vbus
NoConn ~ 1850 3600
Text GLabel 1950 3600 1    50   Input ~ 0
P1_SSRX-
Text GLabel 2050 3600 1    50   Input ~ 0
Vbus
Text GLabel 2150 3600 1    50   Input ~ 0
P1_SSRX+
Text GLabel 2250 3600 1    50   Input ~ 0
P2_SSRX-
$Comp
L power:GND #PWR0101
U 1 1 5FA62C06
P 3250 2900
F 0 "#PWR0101" H 3250 2650 50  0001 C CNN
F 1 "GND" H 3255 2727 50  0000 C CNN
F 2 "" H 3250 2900 50  0001 C CNN
F 3 "" H 3250 2900 50  0001 C CNN
	1    3250 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2350 3600 2350 2900
Text GLabel 2450 3600 1    50   Input ~ 0
P2_SSRX+
Text GLabel 2550 3600 1    50   Input ~ 0
P1_SSTX-
Wire Wire Line
	2650 3600 2650 2900
Wire Wire Line
	2350 2900 2650 2900
Connection ~ 2650 2900
Text GLabel 2750 3600 1    50   Input ~ 0
P1_SSTX+
Text GLabel 2850 3600 1    50   Input ~ 0
P2_SSTX-
Wire Wire Line
	2650 2900 2950 2900
Wire Wire Line
	2950 3600 2950 2900
Text GLabel 3050 3600 1    50   Input ~ 0
P2_SSTX+
Connection ~ 2950 2900
Wire Wire Line
	3250 3600 3250 2900
Connection ~ 3250 2900
Text GLabel 3150 3600 1    50   Input ~ 0
P1_D-
Text GLabel 3450 3600 1    50   Input ~ 0
P2_D-
Text GLabel 3350 3600 1    50   Input ~ 0
P1_D+
Text GLabel 3650 3600 1    50   Input ~ 0
P2_D+
$Comp
L Connector:Conn_01x20_Male J1
U 1 1 5FA5ECEB
P 2650 3800
F 0 "J1" H 2758 4881 50  0000 C CNN
F 1 "Conn_01x20_Male" H 2758 4790 50  0000 C CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_2x10_P2.00mm_Horizontal" H 2650 3800 50  0001 C CNN
F 3 "~" H 2650 3800 50  0001 C CNN
	1    2650 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 2900 3250 2900
Text GLabel 3550 3600 1    50   Input ~ 0
ID
Text GLabel 2200 1450 2    50   Input ~ 0
Vbus
Text GLabel 3600 1450 2    50   Input ~ 0
Vbus
Text GLabel 2200 1650 2    50   Input ~ 0
P1_D-
Text GLabel 2200 1750 2    50   Input ~ 0
P1_D+
Text GLabel 2200 1950 2    50   Input ~ 0
P1_SSRX-
Text GLabel 2200 2050 2    50   Input ~ 0
P1_SSRX+
Text GLabel 2200 2250 2    50   Input ~ 0
P1_SSTX-
Text GLabel 2200 2350 2    50   Input ~ 0
P1_SSTX+
$Comp
L power:GND #PWR0102
U 1 1 5FA7EF7A
P 1600 2550
F 0 "#PWR0102" H 1600 2300 50  0001 C CNN
F 1 "GND" H 1605 2377 50  0000 C CNN
F 2 "" H 1600 2550 50  0001 C CNN
F 3 "" H 1600 2550 50  0001 C CNN
	1    1600 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 2550 1600 2550
Connection ~ 1600 2550
Wire Wire Line
	1700 2550 1600 2550
Text GLabel 3600 1650 2    50   Input ~ 0
P2_D-
Text GLabel 3600 1750 2    50   Input ~ 0
P2_D+
Text GLabel 3600 1950 2    50   Input ~ 0
P2_SSRX-
Text GLabel 3600 2050 2    50   Input ~ 0
P2_SSRX+
Text GLabel 3600 2250 2    50   Input ~ 0
P2_SSTX-
Text GLabel 3600 2350 2    50   Input ~ 0
P2_SSTX+
$Comp
L power:GND #PWR0103
U 1 1 5FA82C68
P 3000 2550
F 0 "#PWR0103" H 3000 2300 50  0001 C CNN
F 1 "GND" H 3005 2377 50  0000 C CNN
F 2 "" H 3000 2550 50  0001 C CNN
F 3 "" H 3000 2550 50  0001 C CNN
	1    3000 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 2550 3000 2550
Connection ~ 3000 2550
Wire Wire Line
	3100 2550 3000 2550
$Comp
L Connector:USB_C_Receptacle USB_C_Out1
U 1 1 5FA8E03B
P 6650 2500
F 0 "USB_C_Out1" H 6757 3767 50  0000 C CNN
F 1 "USB_C_Receptacle" H 6757 3676 50  0000 C CNN
F 2 "CM 690 II USB 3.0 Frontpanel mod:USB_C_Receptacle_U262-241N-4BV60" H 6800 2500 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 6800 2500 50  0001 C CNN
F 4 "C388659" H 6650 2500 50  0001 C CNN "LCSC"
	1    6650 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_C_Plug USB_C_In1
U 1 1 5FA90816
P 5250 2500
F 0 "USB_C_In1" H 5357 3767 50  0000 C CNN
F 1 "USB_C_Plug" H 5357 3676 50  0000 C CNN
F 2 "Connector_USB:USB_C_Plug_Molex_105444" H 5400 2500 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 5400 2500 50  0001 C CNN
F 4 "C561769" H 5250 2500 50  0001 C CNN "LCSC"
	1    5250 2500
	1    0    0    -1  
$EndComp
Text GLabel 5850 1500 2    50   Input ~ 0
C_Vbus
Text GLabel 5850 1700 2    50   Input ~ 0
C_CC
Text GLabel 5850 1800 2    50   Input ~ 0
C_Vcon
Text GLabel 5850 2000 2    50   Input ~ 0
C_D-
Text GLabel 5850 2200 2    50   Input ~ 0
C_D+
Text GLabel 5850 2500 2    50   Input ~ 0
C_RX1-
Text GLabel 5850 2600 2    50   Input ~ 0
C_RX1+
Text GLabel 5850 2800 2    50   Input ~ 0
C_TX1-
Text GLabel 5850 2900 2    50   Input ~ 0
C_TX1+
Text GLabel 5850 3100 2    50   Input ~ 0
C_RX2-
Text GLabel 5850 3200 2    50   Input ~ 0
C_RX2+
Text GLabel 5850 3400 2    50   Input ~ 0
C_TX2-
Text GLabel 5850 3500 2    50   Input ~ 0
C_TX2+
Text GLabel 5850 3700 2    50   Input ~ 0
C_SBU1
Text GLabel 5850 3800 2    50   Input ~ 0
C_SBU2
Text GLabel 7250 1500 2    50   Input ~ 0
C_Vbus
Text GLabel 7250 1700 2    50   Input ~ 0
C_CC
Text GLabel 7250 1800 2    50   Input ~ 0
C_Vcon
Text GLabel 7250 2000 2    50   Input ~ 0
C_D-
Text GLabel 7250 2200 2    50   Input ~ 0
C_D+
Text GLabel 7250 2500 2    50   Input ~ 0
C_RX1-
Text GLabel 7250 2600 2    50   Input ~ 0
C_RX1+
Text GLabel 7250 2800 2    50   Input ~ 0
C_TX1-
Text GLabel 7250 2900 2    50   Input ~ 0
C_TX1+
Text GLabel 7250 3100 2    50   Input ~ 0
C_RX2-
Text GLabel 7250 3200 2    50   Input ~ 0
C_RX2+
Text GLabel 7250 3400 2    50   Input ~ 0
C_TX2-
Text GLabel 7250 3500 2    50   Input ~ 0
C_TX2+
Text GLabel 7250 3700 2    50   Input ~ 0
C_SBU1
Text GLabel 7250 3800 2    50   Input ~ 0
C_SBU2
Text GLabel 7250 2100 2    50   Input ~ 0
C_D-
Text GLabel 7250 2300 2    50   Input ~ 0
C_D+
Text Notes 4100 3950 2    50   ~ 0
Some say, leave ID floating, some say, leave ID grounded
Wire Notes Line
	1150 4400 4300 4400
Wire Notes Line
	1150 1050 4300 1050
Wire Notes Line
	1150 1050 1150 4400
Wire Notes Line
	4300 1050 4300 4400
$Comp
L power:GND #PWR0104
U 1 1 5FAB56CA
P 5250 4100
F 0 "#PWR0104" H 5250 3850 50  0001 C CNN
F 1 "GND" H 5255 3927 50  0000 C CNN
F 2 "" H 5250 4100 50  0001 C CNN
F 3 "" H 5250 4100 50  0001 C CNN
	1    5250 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5FAB5A42
P 6650 4100
F 0 "#PWR0105" H 6650 3850 50  0001 C CNN
F 1 "GND" H 6655 3927 50  0000 C CNN
F 2 "" H 6650 4100 50  0001 C CNN
F 3 "" H 6650 4100 50  0001 C CNN
	1    6650 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5FAB5CA6
P 6350 4100
F 0 "#PWR0106" H 6350 3850 50  0001 C CNN
F 1 "GND" H 6355 3927 50  0000 C CNN
F 2 "" H 6350 4100 50  0001 C CNN
F 3 "" H 6350 4100 50  0001 C CNN
	1    6350 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5FAB608D
P 4950 4100
F 0 "#PWR0107" H 4950 3850 50  0001 C CNN
F 1 "GND" H 4955 3927 50  0000 C CNN
F 2 "" H 4950 4100 50  0001 C CNN
F 3 "" H 4950 4100 50  0001 C CNN
	1    4950 4100
	1    0    0    -1  
$EndComp
Wire Notes Line
	4550 1100 4550 4350
Wire Notes Line
	7800 1100 7800 4350
$Comp
L Connector:AudioJack3_SwitchTR SPK1
U 1 1 5FA4E5A6
P 8650 2000
F 0 "SPK1" H 8632 2325 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 8632 2234 50  0000 C CNN
F 2 "SJ13525NG:SJ1-3525NG" H 8650 2000 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/cui-devices/SJ1-3525NG/738690" H 8650 2000 50  0001 C CNN
	1    8650 2000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x10_Male J3
U 1 1 5FA51045
P 8950 3900
F 0 "J3" H 9058 4481 50  0000 C CNN
F 1 "Conn_01x10_Male" H 9058 4390 50  0000 C CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_2x05_P2.00mm_Horizontal" H 9300 3150 50  0000 C CNN
F 3 "~" H 8950 3900 50  0001 C CNN
	1    8950 3900
	1    0    0    -1  
$EndComp
$Comp
L Connector:AudioJack3_SwitchTR MIC1
U 1 1 5FA50C73
P 10000 2000
F 0 "MIC1" H 9982 2325 50  0000 C CNN
F 1 "AudioJack3_SwitchTR" H 9982 2234 50  0000 C CNN
F 2 "SJ13525NG:SJ1-3525NG" H 10000 2000 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/cui-devices/SJ1-3525NG/738690" H 10000 2000 50  0001 C CNN
	1    10000 2000
	1    0    0    -1  
$EndComp
Text Notes 3300 5050 0    50   ~ 0
I'd rather have two full speed USB Type C connectors on the frontpanel, but one full speed is enough. The other one will only support USB 2.0.\nIf you still want to have two full speed (when a single one is used) USB type C connectors; please use Microchip's USB7252 and TI's HD3SS3220 to create a USB 3.2 Gen 1 SS Hub
$Comp
L Connector:USB_C_Receptacle USB_C_Out2
U 1 1 5FAD8EED
P 2700 5750
F 0 "USB_C_Out2" H 2807 7017 50  0000 C CNN
F 1 "USB_C_Receptacle" H 2807 6926 50  0000 C CNN
F 2 "CM 690 II USB 3.0 Frontpanel mod:USB_C_Receptacle_U262-241N-4BV60" H 2850 5750 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 2850 5750 50  0001 C CNN
F 4 "C388659" H 2700 5750 50  0001 C CNN "LCSC"
	1    2700 5750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x10_Male J2
U 1 1 5FAD9700
P 2450 7100
F 0 "J2" V 2377 7028 50  0000 C CNN
F 1 "Conn_01x10_Male" V 2286 7028 50  0000 C CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_2x05_P2.00mm_Horizontal" H 2450 7100 50  0001 C CNN
F 3 "~" H 2450 7100 50  0001 C CNN
	1    2450 7100
	0    -1   -1   0   
$EndComp
NoConn ~ 2750 6900
Wire Wire Line
	3700 6700 3700 6350
Wire Wire Line
	3100 6350 3100 6550
Wire Wire Line
	3200 6350 3200 6550
Wire Wire Line
	3200 6550 3100 6550
Wire Wire Line
	2900 6450 2900 6350
Wire Wire Line
	2900 6450 3000 6450
Wire Wire Line
	3000 6450 3000 6350
Wire Wire Line
	2450 6450 2900 6450
Wire Wire Line
	2450 6450 2450 6900
Connection ~ 2900 6450
Wire Wire Line
	950  6600 950  6500
Wire Wire Line
	950  5750 1100 5750
Wire Wire Line
	1100 5450 950  5450
Wire Wire Line
	950  5450 950  5750
Connection ~ 950  5750
$Comp
L power:GND #PWR0108
U 1 1 5FAE8274
P 950 6600
F 0 "#PWR0108" H 950 6350 50  0001 C CNN
F 1 "GND" H 955 6427 50  0000 C CNN
F 2 "" H 950 6600 50  0001 C CNN
F 3 "" H 950 6600 50  0001 C CNN
	1    950  6600
	1    0    0    -1  
$EndComp
Text Label 3200 6550 0    50   ~ 0
USB2.0_D-
Text Label 2450 6450 2    50   ~ 0
USB2.0_D+
Text Notes 3400 1000 0    50   ~ 0
USE MOLEX 483930003!
Text Notes 3300 4850 0    50   ~ 0
http://www.winning.com.tw/en/product/product-info.php?id=12930\nis not available, thus we solve it by using a USB C connector internally to allow\nhttps://www.aliexpress.com/item/4001030432499.html\n\nUSB C pinout: https://base.imgix.net/files/base/ebm/mwrf/image/2016/02/mwrf_com_sites_electronicdesign.com_files_uploads_2015_02_0216_TI_USBtypeC_F2_big.png?auto=format&fit=max&w=1440
Wire Wire Line
	3100 6550 2650 6550
Wire Wire Line
	2650 6550 2650 6900
Connection ~ 3100 6550
Wire Wire Line
	2250 6900 2350 6900
Wire Wire Line
	950  6500 2250 6500
Wire Wire Line
	2250 6500 2250 6900
Connection ~ 950  6500
Wire Wire Line
	950  6500 950  5750
Connection ~ 2250 6900
Wire Wire Line
	2850 6900 2950 6900
NoConn ~ 2550 6900
Wire Wire Line
	2950 6900 2950 6700
Connection ~ 2950 6900
Wire Wire Line
	2950 6700 3700 6700
Wire Wire Line
	2050 6900 2150 6900
Text Notes 2600 6850 1    50   ~ 0
D-
Text Notes 2800 6850 1    50   ~ 0
D+
$EndSCHEMATC
